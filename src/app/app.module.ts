import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderMainComponent } from './components/header/header-main/header-main.component';
import { PartnerUnisComponent } from './components/header/partner-unis/partner-unis.component';
import { PersonalInfoComponent } from './components/body/personal-info/personal-info.component';
import { IndexComponent } from './components/index/index.component';
import { VoluntaryInfoComponent } from './components/body/voluntary-info/voluntary-info.component';
import { UndRepComponent } from './components/body/und-rep/und-rep.component';
import { REComponent } from './components/body/re/re.component';
import { MilitaryComponent } from './components/body/military/military.component';
import { DiscSigComponent } from './components/body/disc-sig/disc-sig.component';
import { ApiComponent } from './components/body/api/api.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderMainComponent,
    PartnerUnisComponent,
    PersonalInfoComponent,
    IndexComponent,
    VoluntaryInfoComponent,
    UndRepComponent,
    REComponent,
    MilitaryComponent,
    DiscSigComponent,
    ApiComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
