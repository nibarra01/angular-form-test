import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscSigComponent } from './disc-sig.component';

describe('DiscSigComponent', () => {
  let component: DiscSigComponent;
  let fixture: ComponentFixture<DiscSigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiscSigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscSigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
