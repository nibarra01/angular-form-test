import { Component, Input, OnInit } from '@angular/core';
import { IncompleteForm } from 'src/app/services/form';
import { FormInputsService } from 'src/app/services/form-inputs.service';

@Component({
  selector: 'app-military',
  templateUrl: './military.component.html',
  styleUrls: ['./military.component.css']
})
export class MilitaryComponent implements OnInit {

  currentForm : IncompleteForm;

  active : boolean;
  updateActive(){
    this.form.active(this.active);
    this.milEnd = null;
  }
  retired : boolean;
  updateRetired(){
    this.form.retired(this.retired);
  }
  caBranch : string;
  updateCNbranch(){
    this.form.caBranch(this.caBranch);
    this.usBranch = null;
  }
  milStart : Date;
  updateMilStart(){
    this.form.milStart(this.milStart);
  }
  milEnd : Date;
  updateMilEnd(){
    this.form.milEnd(this.milEnd);
  }
  usBranch : string;
  updateUSbranch(){
    this.form.usBranch(this.usBranch);
    this.caBranch = null;
  }
  rank : string;
  updateRank(){
    this.form.rank(this.rank);
  }
  discharge : string;
  updateDischarge(){
    this.form.discharge(this.discharge);
  }
  dischargeReason : string;
  updateDischargeReason(){
    this.form.dischargeReason(this.dischargeReason);
  }


  military : boolean = this.form.currentForm.value.military;
  country : string = this.form.currentForm.value.country;
  constructor(public form:FormInputsService) { } 

  ngOnInit(): void {
    this.subscribe();



  }

  subscribe(){
    this.form.formStart().subscribe(f => this.currentForm = f);
  }

  num(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    if(charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }
    return true;
  }

  letter(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    if(charCode > 31 && ((charCode < 65 || charCode > 90))){
      if (charCode < 97 || charCode > 122){
        return false;
      }
    }
    return true;
  }

  valid(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    let number:boolean = this.num(e);
    let letter:boolean = this.letter(e);
    let otherAccepted:boolean = (charCode > 31 && charCode < 48) || charCode == 58 || charCode == 59 || charCode == 63 || charCode == 64;

    if (number || letter || otherAccepted){
      return true;
    } else {
      return false;
    }
  }
}
