import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { IncompleteForm } from 'src/app/services/form';
import { FormInputsService } from 'src/app/services/form-inputs.service';
import { StringLiteral } from 'typescript';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {

  

  country : string;
  updateCountry(){
    this.form.country(this.country);
    if (this.country == "USA"){
      this.province =null;
      this.sin1 = null;
      this.sin2 = null;
      this.sin3 = null;
    }
    if (this.country == "CAN"){
      this.state = null;
      this.county = null;
      this.ssn1 = null;
      this.ssn2 = null;
      this.ssn3 = null;
    }
  }
  formDate : Date;
  updateDate(){
    this.form.formDate(this.formDate);
  }
  firstName : string;
  updateFirstName(){
    this.form.firstName(this.firstName);
  }
  middleName : string;
  updateMiddleName(){
    this.form.middleName(this.middleName);
  }
  middleClear(){
    this.form.middleClear();
    this.middleName = null; 
  }
  lastName : string;
  updateLastName(){
    this.form.lastName(this.lastName);
  }
  midbox : boolean = false;

  number1 : number;
  number2 : number;
  number3 : number;
  updatePhone(){
    this.form.phoneNumber(this.number1,this.number2,this.number3);
  }
  email : string;
  updateEmail(){
    this.form.email(this.email);
  }
  street : string;
  updateStreet(){
    this.form.street(this.street);
  }
  apt : string;
  updateApt(){
    this.form.apt(this.apt);
  }
  clearApt(){
    this.form.aptClear();
    this.apt = null;
  }
  city: string;
  updateCity(){
    this.form.city(this.city);
  }
  county : string;
  updateCounty(){
    this.form.county(this.county);
  }
  state : string;
  updateState(){
    this.form.state(this.state);
  }
  province : string;
  updateProvince(){
    this.form.province(this.province);
  }
  aptbox : boolean = false;

  dob : Date;
  updateDOB(){
    this.form.dob(this.dob);
  }
  ssn1 : number;
  ssn2 : number;
  ssn3 : number;
  updateSSN(){
    this.form.ssn(this.ssn1,this.ssn2,this.ssn3);
  }
  sin1 : number;
  sin2 : number;
  sin3 : number;
  updateSIN(){
    this.form.sin(this.sin1,this.sin2,this.sin3)
  }

  uscitizen : boolean;
  updateCitizen(){
    this.form.usCitizen(this.uscitizen);
  }
  usworkauth : boolean;
  updateWorkAuth(){
    this.form.usWorkAuth(this.usworkauth);
  }

  emName : string;
  updateEmName(){
    this.form.emName(this.emName);
  }
  emNum1a : number;
  emNum1b : number;
  emNum1c : number;
  updateEmN1(){
    this.form.emNum1(this.emNum1a,this.emNum1b,this.emNum1c)
  }
  emNum2a : number;
  emNum2b : number;
  emNum2c : number;
  updateEmN2(){
    this.form.emNum2(this.emNum2a,this.emNum2b,this.emNum2c)
  }
  emRel : string;
  updateEmRel(){
    this.form.emRel(this.emRel)
  }

  location : string;
  updateLocation(){
    this.form.location(this.location)
  }



  dateType : string;
  today : Date;
  currentForm : IncompleteForm;
  constructor(private form:FormInputsService) { } 


  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.form.formStart().subscribe(f => this.currentForm = f);
  }
  setToday(){
    this.formDate = this.today;
  }

  num(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    if(charCode > 31 && (charCode < 48 || charCode > 57)){
      return false;
    }
    return true;
  }

  letter(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    if(charCode > 31 && ((charCode < 65 || charCode > 90))){
      if (charCode < 97 || charCode > 122){
        return false;
      }
    }
    return true;
  }

  address(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    let number:boolean = this.num(e);
    let letter:boolean = this.letter(e);
    let otherAccepted:boolean = (charCode == 32 || charCode == 35 || charCode == 45);

    if (number || letter || otherAccepted){
      return true;
    } else {
      return false;
    }
  }

  emailInput(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    let number:boolean = this.num(e);
    let letter:boolean = this.letter(e);
    let otherAccepted:boolean = (charCode == 32 || charCode == 46 || charCode == 45 || charCode == 64 || charCode == 95);

    if (number || letter || otherAccepted){
      return true;
    } else {
      return false;
    }
  }
}
