import { Component, OnInit } from '@angular/core';
import { IncompleteForm } from 'src/app/services/form';
import { FormInputsService } from 'src/app/services/form-inputs.service';

@Component({
  selector: 'app-re',
  templateUrl: './re.component.html',
  styleUrls: ['./re.component.css']
})
export class REComponent implements OnInit {
  racEth : string;
  update(){
    this.form.RE(this.racEth);
  }

  currentForm : IncompleteForm;
  constructor(private form:FormInputsService) { } 

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.form.formStart().subscribe(f => this.currentForm = f);
  }

}
