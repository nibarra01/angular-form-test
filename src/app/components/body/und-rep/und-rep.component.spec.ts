import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UndRepComponent } from './und-rep.component';

describe('UndRepComponent', () => {
  let component: UndRepComponent;
  let fixture: ComponentFixture<UndRepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UndRepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UndRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
