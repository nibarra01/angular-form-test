import { Component, OnInit } from '@angular/core';
import { format } from 'path';
import { IncompleteForm } from 'src/app/services/form';
import { FormInputsService } from 'src/app/services/form-inputs.service';

@Component({
  selector: 'app-und-rep',
  templateUrl: './und-rep.component.html',
  styleUrls: ['./und-rep.component.css']
})
export class UndRepComponent implements OnInit {

  military : boolean;
  updateMilitary(){
    this.form.military(this.military);
  }
  firstResponder : boolean;
  updateFR(){
    this.form.firstResponder(this.firstResponder);
  }
  femstem : boolean;
  updateFemstem(){
    this.form.femstem(this.femstem);
  }
  upskill : boolean;
  updateUpskill(){
    this.form.upskill(this.upskill);
  }
  careerChange : boolean;
  updateChange(){
    this.form.career(this.careerChange);
  }
  vidya : boolean;
  updateVidya(){
    this.form.vidya(this.vidya);
  }
  lgbtq : boolean;
  updateLgbtq(){
    this.form.lgbtq(this.lgbtq)
  }
  firstgen : boolean;
  updateFirstGen(){
    this.form.firstgen(this.firstgen);
  }
  minStem : boolean;
  updateMinStem(){
    this.form.minstem(this.minStem);
  }
  singleParent : boolean;
  updateSingleParent(){
    this.form.singleParent(this.singleParent);
  }

  currentForm : IncompleteForm;
  constructor(private form:FormInputsService) { } 

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.form.formStart().subscribe(f => this.currentForm = f);
  }


}
