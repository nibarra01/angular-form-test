import { Component, OnInit } from '@angular/core';
import { IncompleteForm } from 'src/app/services/form';
import { FormInputsService } from 'src/app/services/form-inputs.service';

@Component({
  selector: 'app-voluntary-info',
  templateUrl: './voluntary-info.component.html',
  styleUrls: ['./voluntary-info.component.css']
})
export class VoluntaryInfoComponent implements OnInit {
  gender : string;
  other : string;
  updateGender(){
    this.form.gender(this.gender);
    console.log(this.gender)
  }
  marStat : string;
  updateMarStat(){
    this.form.marital(this.marStat);
  }

  currentForm : IncompleteForm;
  constructor(private form:FormInputsService) { } 

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.form.formStart().subscribe(f => this.currentForm = f);
  }

  valid(e):boolean{
    const charCode = (e.which)?e.which:e.keycode;
    if(charCode > 32 && ((charCode < 65 || charCode > 90)) && charCode != 45){
      if (charCode < 97 || charCode > 122){
        return false;
      }
    }
    return true;
  }
}
