import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerUnisComponent } from './partner-unis.component';

describe('PartnerUnisComponent', () => {
  let component: PartnerUnisComponent;
  let fixture: ComponentFixture<PartnerUnisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerUnisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerUnisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
