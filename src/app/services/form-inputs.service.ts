import { PathLocationStrategy } from '@angular/common';
import { NullTemplateVisitor } from '@angular/compiler';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { AttachSession } from 'protractor/built/driverProviders';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IncompleteForm } from './form';

@Injectable({
  providedIn: 'root'
})
export class FormInputsService {
  currentForm : BehaviorSubject<IncompleteForm>;


  constructor() { 
    this.currentForm = new BehaviorSubject<IncompleteForm>(
      {
        country : null,
        formDate : null,
        firstName : null,
        middleName : null,
        lastName : null,
      
        phoneNumber : null,
        email : null,
      
        street : null,
        apt : null,
        city : null,
        county : null,
        state : null,
        province : null,
    
        dob : null,
        
        ssn : null,
        sin : null,
      
        uscitizen : null,
        usworkauth : null,
      
        emName : null,
        emNumber1 : null,
        emNumber2 : null,
        emRel : null,
    
        location : null,
        // 
    
        gender : null,
        marStat : null,
    
        // 
    
        racEth : null,
    
        // 
    
        military : null,
        firstResponder : null,
        femstem : null,
        upskill : null,
        careerChange : null,
        vidya : null,
        lgbtq : null,
        firstgen : null,
        minStem : null,
        singleParent : null,

        // 

        active : null,
        retired : null,
        usBranch : null,
        caBranch : null,
        rank : null,
        milStart : null,
        milEnd : null,
        discharge : null,
        dischargeReason : null
      }
    );
   }

  formStart(): Observable<IncompleteForm>{
    return this.currentForm.asObservable();
  }

  // 

  firstName(x:string){
    this.currentForm.value.firstName = x;
    this.currentForm.next(this.currentForm.value);
  }

  lastName(x:string){
    this.currentForm.value.lastName = x;
    this.currentForm.next(this.currentForm.value);

  }

  middleName(x:string){
    this.currentForm.value.middleName = x;
    this.currentForm.next(this.currentForm.value);

  }
  middleClear(){
    this.currentForm.value.middleName = null;
    this.currentForm.next(this.currentForm.value);

  }
  country(x:string){
    this.currentForm.value.country = x;
    this.currentForm.next(this.currentForm.value);
    if(x=="USA"){
      this.currentForm.value.province = null;
      this.currentForm.value.sin = null;
    }
    if(x=="CAN"){
      this.currentForm.value.state = null;
      this.currentForm.value.county = null;
      this.currentForm.value.ssn = null;
    }
    this.currentForm.value.rank = null;
  }

  formDate(x:Date){
    this.currentForm.value.formDate = x;
    this.currentForm.next(this.currentForm.value);

  }
  phoneNumber(x:number,y:number,z:number){
    if(x != null && y != null && z != null){
      let num : string = x.toString() + y.toString() + z.toString();
    let phone:number = parseInt(num);
    this.currentForm.value.phoneNumber = phone;
    this.currentForm.next(this.currentForm.value);

    }
  }

  dob(x:Date){
    this.currentForm.value.dob = x;
    this.currentForm.next(this.currentForm.value);

  }
  email(x:string){
    this.currentForm.value.email = x;
    this.currentForm.next(this.currentForm.value);

  }
  street(x:string){
    this.currentForm.value.street = x;
    this.currentForm.next(this.currentForm.value);

  }
  apt(x:string){
    this.currentForm.value.apt = x;
    this.currentForm.next(this.currentForm.value);

  }
  aptClear(){
    this.currentForm.value.apt = null;
    this.currentForm.next(this.currentForm.value);

  }
  city(x:string){
    this.currentForm.value.city = x;
    this.currentForm.next(this.currentForm.value);

  }
  state(x:string){
    this.currentForm.value.state = x;
    this.currentForm.next(this.currentForm.value);

  }
  county(x:string){
    this.currentForm.value.county = x;
    this.currentForm.next(this.currentForm.value);

  }
  province(x:string){
    this.currentForm.value.province = x;
    this.currentForm.next(this.currentForm.value);

  }
  usCitizen(x:boolean){
    this.currentForm.value.uscitizen = x;
    this.currentForm.next(this.currentForm.value);

  }
  usWorkAuth(x:boolean){
    this.currentForm.value.usworkauth = x;
    this.currentForm.next(this.currentForm.value);

  }
  ssn(x:number,y:number,z:number){
    if(x != null && y != null && z != null){
      let str : string = x.toString() + y.toString() + z.toString();
    let num:number = parseInt(str);
    this.currentForm.value.ssn = num;
    this.currentForm.next(this.currentForm.value);

    }
  }
  sin(x:number,y:number,z:number){
    if(x != null && y != null && z != null){
      let str : string = x.toString() + y.toString() + z.toString();
    let num:number = parseInt(str);
    this.currentForm.value.sin = num;
    this.currentForm.next(this.currentForm.value);

    }
  }
  emName(x:string){
    this.currentForm.value.emName = x;
    this.currentForm.next(this.currentForm.value);

  }
  emRel(x:string){
    this.currentForm.value.emRel = x;
    this.currentForm.next(this.currentForm.value);

  }
  emNum1(x:number,y:number,z:number){
    if(x != null && y != null && z != null){
      let num : string = x.toString() + y.toString() + z.toString();
    let phone:number = parseInt(num);
    this.currentForm.value.emNumber1 = phone;
    this.currentForm.next(this.currentForm.value);

  }
}
  emNum2(x:number,y:number,z:number){
    if(x != null && y != null && z != null){
      let num : string = x.toString() + y.toString() + z.toString();
    let phone:number = parseInt(num);
    this.currentForm.value.emNumber2 = phone;
    this.currentForm.next(this.currentForm.value);

    }

  }

  location(x:string){
    this.currentForm.value.location = x;
    this.currentForm.next(this.currentForm.value);

  }

gender(x:string){
  this.currentForm.value.gender = x;
  this.currentForm.next(this.currentForm.value);

}
marital(x:string){
  this.currentForm.value.marStat = x;
  this.currentForm.next(this.currentForm.value);
}

RE(re:string){
  this.currentForm.value.racEth = re;
  this.currentForm.next(this.currentForm.value);

}
military(x:boolean){
  this.currentForm.value.military = x;
  this.currentForm.next(this.currentForm.value);

}
firstResponder(x:boolean){
  this.currentForm.value.firstResponder =x;
  this.currentForm.next(this.currentForm.value);

}
femstem(x:boolean){
  this.currentForm.value.femstem = x;
  this.currentForm.next(this.currentForm.value);

}
upskill(x:boolean){
  this.currentForm.value.upskill = x ;
  this.currentForm.next(this.currentForm.value);

}
career(x:boolean){
  this.currentForm.value.careerChange = x;
  this.currentForm.next(this.currentForm.value);

}
vidya(x:boolean){
  this.currentForm.value.vidya = x;
  this.currentForm.next(this.currentForm.value);

}
lgbtq(x:boolean){
  this.currentForm.value.lgbtq = x;
  this.currentForm.next(this.currentForm.value);

}
firstgen(x:boolean){
  this.currentForm.value.firstgen = x;
  this.currentForm.next(this.currentForm.value);

}
minstem(x:boolean){
  this.currentForm.value.minStem = x;
  this.currentForm.next(this.currentForm.value);

}
singleParent(x:boolean){
  this.currentForm.value.singleParent = x;
  this.currentForm.next(this.currentForm.value);

}


active(x:boolean){
  this.currentForm.value.active = x;
  this.currentForm.value.milEnd = null;
  this.currentForm.next(this.currentForm.value);

}
retired(x:boolean){
  this.currentForm.value.retired = x;
  this.currentForm.next(this.currentForm.value);

}
usBranch(x:string){
  this.currentForm.value.usBranch = x;
  this.currentForm.value.caBranch = null;
  this.currentForm.next(this.currentForm.value);

}
caBranch(x:string){
  this.currentForm.value.caBranch = x;
  this.currentForm.value.usBranch = null;
  this.currentForm.next(this.currentForm.value);

}
rank(x:string){
  this.currentForm.value.rank = x;
  this.currentForm.next(this.currentForm.value);

}
milStart(x:Date){
  this.currentForm.value.milStart = x;
  this.currentForm.next(this.currentForm.value);

}
milEnd(x:Date){
  this.currentForm.value.milEnd = x;
  this.currentForm.next(this.currentForm.value);

}
discharge(x:string){
  this.currentForm.value.discharge = x;
  this.currentForm.next(this.currentForm.value);

}
dischargeReason(x:string){
  this.currentForm.value.dischargeReason = x;
  this.currentForm.next(this.currentForm.value);

}

getCountry(){
  return this.currentForm.value.country;
}

}