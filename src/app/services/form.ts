export interface IncompleteForm{
    country ?: string;
    formDate ?: Date;
    firstName ?: string;
    middleName ?: string;
    lastName ?: string;
  
    phoneNumber ?: number;
    email ?: string;
  
    street ?: string;
    apt ?: string;
    city ?: string;
    county ?: string;
    state ?: string;
    province ?: string;

    dob ?: Date;
    
    ssn ?: number;
    sin ?: number;
  
    uscitizen ?: boolean;
    usworkauth ?: boolean;
  
    emName ?: string;
    emNumber1 ?: number;
    emNumber2 ?: number;
    emRel ?: string;

    location ?: string;
    // 

    gender ?: string;
    marStat ?: string;

    // 

    racEth ?: string;

    // 

    military ?: boolean;
    firstResponder ?: boolean;
    femstem ?: boolean;
    upskill ?: boolean;
    careerChange ?: boolean;
    vidya ?: boolean;
    lgbtq ?: boolean;
    firstgen ?: boolean;
    minStem ?: boolean;
    singleParent ?: boolean;

    active ?: boolean;
    retired ?: boolean;
    usBranch ?: string;
    caBranch ?: string;
    rank ?: string;
    milStart ?: Date;
    milEnd ?: Date;
    discharge ?: string;
    dischargeReason ?: string;

}